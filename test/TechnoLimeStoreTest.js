const { expect } = require("chai");
const { ethers } = require("hardhat");

async function deployContractByName(name) {
  const contract = await ethers.getContractFactory(name);
  const deployedContract = await contract.deploy();

  await deployedContract.deployed();

  return deployedContract;
}

describe("TechnoLimeStore", function () {
  it("check if initial balance is 0", async function () {
    const TechnoLimeStore = await deployContractByName("TechnoLimeStore");

    const balance = await TechnoLimeStore.balanceOfContract();
    expect(balance).to.equal(0);
  });

  it("should create a product", async function () {
    const TechnoLimeStore = await deployContractByName("TechnoLimeStore");

    const tx = await TechnoLimeStore.createProduct("100", "33", ethers.utils.parseUnits("1", "ether"));
    const product = await tx.wait();

    expect(product).to.be.an("Object");

    const event = product.events.find(event => event.event === "ProductCreated");

    expect(event).to.not.be.undefined;
    expect(event.args).to.be.an("Array");
    expect(event.args).to.haveOwnProperty("_identifier");
    expect(event.args._identifier).to.equal("100");
    expect(event.args).to.haveOwnProperty("_quantity");
    expect(event.args._quantity).to.equal("33");
    expect(event.args).to.haveOwnProperty("_priceInWei");
    expect(event.args._priceInWei).to.equal(ethers.utils.parseUnits("1", "ether"));
  });

  it("should not be able to create a same product twice", async function () {
    const TechnoLimeStore = await deployContractByName("TechnoLimeStore");

    const tx = await TechnoLimeStore.createProduct("100", "33", ethers.utils.parseUnits("1", "ether"));
    await tx.wait();

    await expect(TechnoLimeStore.createProduct("100", "33", ethers.utils.parseUnits("1", "ether")))
      .to.be.revertedWith("The product already exists");
  });

  it("should not be able to create a product with a non-owner address", async function () {
    const TechnoLimeStore = await deployContractByName("TechnoLimeStore");
    const [owner, addr1] = await ethers.getSigners();

    await expect(TechnoLimeStore.connect(addr1).createProduct("100", "33", ethers.utils.parseUnits("1", "ether")))
      .to.be.revertedWith("Ownable: caller is not the owner");
  });

  it("add quantity to product", async function () {
    const TechnoLimeStore = await deployContractByName("TechnoLimeStore");
    await TechnoLimeStore.createProduct("100", "33", ethers.utils.parseUnits("1", "ether"));

    await TechnoLimeStore.addQuantityToProduct("100", "100");

    const updatedProduct = await TechnoLimeStore.products(100);
    expect(updatedProduct).to.be.an("Array");
    expect(updatedProduct).to.haveOwnProperty("_identifier");
    expect(updatedProduct._identifier).to.equal("100");
    expect(updatedProduct).to.haveOwnProperty("_quantity");
    expect(updatedProduct._quantity).to.equal("133");
    expect(updatedProduct).to.haveOwnProperty("_priceInWei");
    expect(updatedProduct._priceInWei).to.equal(ethers.utils.parseUnits("1", "ether"));
  });

  it("should not be able to update quantity with a non-owner address", async function () {
    const TechnoLimeStore = await deployContractByName("TechnoLimeStore");
    const [owner, addr1] = await ethers.getSigners();
    await TechnoLimeStore.createProduct("100", "33", ethers.utils.parseUnits("1", "ether"));

    await expect(TechnoLimeStore.connect(addr1).addQuantityToProduct("100", "33"))
      .to.be.revertedWith("Ownable: caller is not the owner");
  });

  it("should not be able to update quantity of non-existing product", async function () {
    const TechnoLimeStore = await deployContractByName("TechnoLimeStore");
    await expect(TechnoLimeStore.addQuantityToProduct("100", "33"))
      .to.be.revertedWith("The product doesn't exist");
  });

  it("should create a purchase", async function () {
    const [owner] = await ethers.getSigners();
    const TechnoLimeStore = await deployContractByName("TechnoLimeStore");
    await TechnoLimeStore.createProduct("100", "33", ethers.utils.parseUnits("1", "ether"));

    const quantityBefore = (await TechnoLimeStore.products("100"))._quantity;
    const buyersBefore = (await TechnoLimeStore.buyers("100", owner.address));
    expect(buyersBefore).to.equal(0);

    const tx = await TechnoLimeStore.createPurchase("100");
    const purchase = await tx.wait();
    expect(purchase).to.be.an("Object");

    const event = purchase.events.find(event => event.event === "PurchaseCreated");

    expect(event).to.not.be.undefined;
    expect(event.args).to.be.an("Array");
    expect(event.args).to.haveOwnProperty("_addr");

    const quantityAfter = (await TechnoLimeStore.products("100"))._quantity;
    expect(quantityAfter).to.equal(quantityBefore - 1);

    const buyersAfter = (await TechnoLimeStore.buyers("100", owner.address));
    expect(buyersAfter).to.equal(1);
  });

  it("should not be able ot create a purchase with non-existing identifier", async function () {
    const TechnoLimeStore = await deployContractByName("TechnoLimeStore");
    await expect(TechnoLimeStore.createPurchase("100"))
      .to.be.revertedWith("The product doesn't exist");
  });

  it("should make a payment", async function () {
    const [owner, addr1] = await ethers.getSigners();
    const TechnoLimeStore = await deployContractByName("TechnoLimeStore");
    const fundingTxHash = await owner.sendTransaction({
      to: TechnoLimeStore.address,
      value: ethers.utils.parseUnits("1", "ether").toHexString()
    });

    await TechnoLimeStore.createProduct("100", "33", ethers.utils.parseUnits("1", "ether"));

    const tx = await TechnoLimeStore.connect(addr1).createPurchase("100");
    const purchase = await tx.wait();
    const event = purchase.events.find(event => event.event === "PurchaseCreated");
    const purchaseAddr = event.args._addr;

    const payment = await addr1.sendTransaction({
      to: purchaseAddr,
      value: ethers.utils.parseUnits("1", "ether").toHexString()
    });

    expect(await TechnoLimeStore.balanceOfContract()).to.equal(ethers.utils.parseUnits("2", "ether"));
  });

  it("should not be able to make a payment with different amount", async function () {
    const [owner, addr1] = await ethers.getSigners();
    const TechnoLimeStore = await deployContractByName("TechnoLimeStore");

    await TechnoLimeStore.createProduct("100", "33", ethers.utils.parseUnits("1", "ether"));

    const tx = await TechnoLimeStore.connect(addr1).createPurchase("100");
    const purchase = await tx.wait();
    const event = purchase.events.find(event => event.event === "PurchaseCreated");
    const purchaseAddr = event.args._addr;

    await expect(addr1.sendTransaction({
      to: purchaseAddr,
      value: ethers.utils.parseUnits("2", "ether").toHexString()
    })).to.be.revertedWith("Ether amount should be exact as the product price");
  });

  it("should make a refund", async function () {
    const [owner, addr1] = await ethers.getSigners();
    const TechnoLimeStore = await deployContractByName("TechnoLimeStore");
    const fundingTxHash = await owner.sendTransaction({
      to: TechnoLimeStore.address,
      value: ethers.utils.parseUnits("1", "ether").toHexString()
    });

    await TechnoLimeStore.createProduct("100", "33", ethers.utils.parseUnits("1", "ether"));

    const tx = await TechnoLimeStore.connect(addr1).createPurchase("100");
    const purchase = await tx.wait();
    const event = purchase.events.find(event => event.event === "PurchaseCreated");
    const purchaseAddr = event.args._addr;

    const payment = await addr1.sendTransaction({
      to: purchaseAddr,
      value: ethers.utils.parseUnits("1", "ether").toHexString()
    });

    const PurchaseContract = await hre.ethers.getContractAt("Purchase", purchaseAddr);
    expect(await PurchaseContract.state()).to.equal(1);
    expect(await TechnoLimeStore.balanceOfContract()).to.equal(ethers.utils.parseUnits("2", "ether"));

    await PurchaseContract.connect(addr1).cancelPurchase();

    expect(await PurchaseContract.state()).to.equal(2);
    expect(await TechnoLimeStore.balanceOfContract()).to.equal(ethers.utils.parseUnits("1", "ether"));
  });

  it("should not be able to make a refund if not the owner", async function () {
    const [owner, addr1] = await ethers.getSigners();
    const TechnoLimeStore = await deployContractByName("TechnoLimeStore");

    await TechnoLimeStore.createProduct("100", "33", ethers.utils.parseUnits("1", "ether"));

    const tx = await TechnoLimeStore.connect(addr1).createPurchase("100");
    const purchase = await tx.wait();
    const event = purchase.events.find(event => event.event === "PurchaseCreated");
    const purchaseAddr = event.args._addr;

    const payment = await addr1.sendTransaction({
      to: purchaseAddr,
      value: ethers.utils.parseUnits("1", "ether").toHexString()
    });

    const PurchaseContract = await hre.ethers.getContractAt("Purchase", purchaseAddr);

    await expect(PurchaseContract.cancelPurchase())
      .to.be.revertedWith("Only purchase creator can cancel the order");
  });

  it("should not be able to make a refund for unpaid purchase", async function () {
    const [owner, addr1] = await ethers.getSigners();
    const TechnoLimeStore = await deployContractByName("TechnoLimeStore");

    await TechnoLimeStore.createProduct("100", "33", ethers.utils.parseUnits("1", "ether"));

    const tx = await TechnoLimeStore.connect(addr1).createPurchase("100");
    const purchase = await tx.wait();
    const event = purchase.events.find(event => event.event === "PurchaseCreated");
    const purchaseAddr = event.args._addr;

    const PurchaseContract = await hre.ethers.getContractAt("Purchase", purchaseAddr);

    await expect(PurchaseContract.connect(addr1).cancelPurchase())
      .to.be.revertedWith("Only paid purchases can be cancelled");
  });
});
